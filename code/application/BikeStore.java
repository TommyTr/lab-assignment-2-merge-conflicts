//Tommy Tran
//1435162
package application;
import vehicles.Bicycle;

public class BikeStore{

    public static void main(String[] args){

        Bicycle[] listBicycles=new Bicycle[4];

        listBicycles[0]=new Bicycle("A2B",20,40);
        listBicycles[1]=new Bicycle("Benno",25,50);
        listBicycles[2]=new Bicycle("Cannondale",30,60);
        listBicycles[3]=new Bicycle("Dahon",35,70);

        for(Bicycle bicycle:listBicycles){
            System.out.println(bicycle);
        }
    }
}